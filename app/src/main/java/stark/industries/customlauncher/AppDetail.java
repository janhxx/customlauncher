package stark.industries.customlauncher;

import android.graphics.drawable.Drawable;

/**
 * Created by Jayswagstar on 24.03.2016.
 */

public class AppDetail {
    CharSequence label;
    CharSequence name;
    Drawable icon;
}

