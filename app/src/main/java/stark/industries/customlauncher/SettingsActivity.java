package stark.industries.customlauncher;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Jayswagstar on 24.03.2016.
        */

public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    public void showHiddenApps(View v){
        startActivity(new Intent(this, HiddenAppsListActivity.class));
    }
}
